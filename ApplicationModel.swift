//Print only files that you have created or modified. Highlight your modifications.
//
//You are to develop a Single View iOS application that should satisfy
//the following requirements:
//
//You are expected to start with an incomplete application (zip file) available
//at /img204/solutions/Assignment8 .
//The application uses UITableView to provide information about a randomly
//generated class of 1000 students. Complete the application so that
//the results of each method (below the comment TO BE COMPLETED in
//the ApplicationModel class) are displayed when the appropriate
//action is selected in the UIPickerView and
//the user clicks on the Execute Action button.
//Modify the project so that it uses the Fakery cocoapod
//to generate the data inside the Generator class.

//Thais Lescano

import Foundation

class ApplicationModel {
  // number of records to create
  let MAX:Int = 10
  private var studentList: [ String : Student] = [:]
  var keys:[String] = []
  var generator: Generator
  
  init()
  {
    self.studentList = [:]
    self.keys = [""]
    generator = Generator()
  }
  
  
  
  
  func generateNewData() -> [String]
  {
    var data:[String] = generator.generateMultiplePersonRecords(howMany: MAX)
    var strResults: [String] = []
    
    var counter:Int = 0
    
    for _ in data
    {
      var strStudent:[String] = data[counter].components(separatedBy: "|")
      var gradeArray:[Int] = []
      for counter1 in 8 ..< strStudent.count - 1
      {
        gradeArray.append(Int(strStudent[counter1])!)
      }
      let student:Student =
      Student(md5: strStudent[0], firstName: strStudent[1],
              lastName: strStudent[2], birthYear: Int(strStudent[3])!,
              street: strStudent[4], streetNumber: Int(strStudent[5])!,
              city: strStudent[6], province: strStudent[7], grades: gradeArray)!
      
      counter += 1
      self.keys.append(student.md5)
      self.studentList[student.md5] = student
      strResults.append(formatInt(int: student.birthYear, width:4) + " | " +
        formatStringWidth(string: student.city, width: 14) + " | " +
        formatStringWidth(string: student.lastName, width: 8) + " | " +
        formatStringWidth(string: student.firstName, width: 6) + " | " +
        formatStringWidth(string: student.province, width: 2) + " | " +
        formatStringWidth(string: student.street, width: 8)
      )
    }
    return strResults
  }
  
  // create a method "getSortedArrayOfAllCities" with NO REPEATED cities
  func getSortedArrayOfAllCities() -> [String]
  {
    var cities = Set<String>()
    for (_, student) in studentList
    {
      cities.insert(student.city)
    }
    
    let cityArray:[String] = Array(cities)
    let sorted = cityArray.sorted(by: <)
    return sorted
  }
  
  // create a method "getSortedArrayOfAllProvinces" with NO REPEATED provinces
  func getSortedArrayOfAllProvinces() -> [String]
  {
    var prov = Set<String>()
    for(_, stud) in studentList
    {
      prov.insert(stud.province)
    }
    
    let provArray:[String] = Array(prov)
    let sorted = provArray.sorted(by: <)
    return sorted
  }
  
  // create a method "getAllStudentsFromComox"
  
  func getAllStudentsFromComox () -> [String] {
    var students = Set<String>()
    for (_, student) in studentList
    {
      if (student.city) == "Comox" {
        students.insert(student.firstName)
      }
    }
    let ComoxStudents:[String] = Array(students)
    
    return ComoxStudents
  }
  
  // create a method "getStudentsSortedByAverageGrade"
  func getStudentsSortedByAverageGrade () -> [String] {
    
    var averageGrades = [String]()
    var average = 0
    var name = ""
    var gradesRecorded : Int
    for (_, student) in studentList
    {
      var sum = 0
      if (!student.grades.isEmpty){
        for grade in student.grades {
          sum = sum + grade
        }
        gradesRecorded = student.grades.count
        name = student.firstName
      }
      else {
        gradesRecorded = 1
      }
      average = sum/gradesRecorded
      averageGrades.append(String(average)+" "+name)
    }
    return averageGrades.sorted(by: > )
  }
  
  //get students sorted by last name
  func getStudentsSortedByLastName () -> [String] {
    var lastNames = Set<String>()
    for(_, student) in studentList
    {
      lastNames.insert(student.lastName + " " + student.firstName)
    }
    let namesArray:[String] = Array(lastNames)
    let sortedList = namesArray.sorted(by: <)
    return sortedList
  }
  
  // gets 3 best students
  func get3BestStudents () -> [String] {
   
    let averages = getStudentsSortedByAverageGrade()
    var bestStudents = [String]()
    
    bestStudents.append(averages[0])
    bestStudents.append(averages[1])
    bestStudents.append(averages[2])
    
    return bestStudents
  }
  


  //  get 3 weakest students
func get3WeakestStudents () -> [String] {
  let averages = getStudentsSortedByAverageGrade()
  var weakStudents = [String]()
  let sorted = averages.sorted(by: < )
  
  weakStudents.append(sorted[0])
  weakStudents.append(sorted[1])
  weakStudents.append(sorted[2])
  
  
  
    return weakStudents
 }
  // create a method "getStudentsSortedByAge"
  func getStudentsSortedByAge() -> [String]
  {
    var ages = [String]()
    
    for (_, student) in studentList
    {
      let name = student.firstName + " " + student.lastName
      let age = String(2016 - student.birthYear)
      ages.append(age + " " + name)
    }
    return ages.sorted(by: <  )
  }
  
  // create a method "getStudentsFromBC"
  func getAllStudentsFromBC () -> [String] {
    var studentsBC = [String] ()
    for (_, student) in studentList
    {
      if (student.province == "BC")
      {
        let fullname = student.firstName + " " + student.lastName
        studentsBC.append(fullname)
      }
    }
    return studentsBC
  }
  
  
  // create a method "getCitiesSortedByNumberOfStudents"
  func getCitiesSortedByNumberOfStudents () -> [String] {
    
    var number = 1
    var citiesAndNumb = [String]()
    var cities = [String: Int]()
    cities["Comox"] = 0
    cities["Courtenay"] = 0
    cities["Cumberland"] = 0
    cities["Campbell River"] = 0
    cities["Nanaimo"] = 0
    cities["Port Hardy"] = 0
    cities["Port Alberni"] = 0
    cities["Prague"] = 0
    cities["Paris"] = 0
    cities["LA"] = 0
    cities["Berlin"] = 0
    cities["Rome"] = 0
    cities["Edmonton"] = 0
    cities["Calgary"] = 0
    cities["Red Deer"] = 0
    cities["Brno"] = 0
    cities["Ostrave"] = 0
    cities["Wienna"] = 0
    cities["Dobra"] = 0
    cities["Presov"] = 0
    cities["Bardejov"] = 0
    cities["Hamburg"] = 0
    cities["London"] = 0
    cities["Plosina"] = 0
    cities["Victoria"] = 0
    
    for (_, student) in studentList
    {
      for (city,_) in cities {
        if (student.city == city) {
          cities.updateValue(number, forKey: city)
        }
      }
    }
    number = number + 1
    for (cityName, number) in cities {
      citiesAndNumb.append(cityName + " " + String(number))
    }
    return citiesAndNumb.sorted(by: > )
  }
  
  // create a method "removeAllStudentsFromBC"
  func removeAllStudentsFromBC () -> [String] {
    var noBC = [String]()
    for (key, student) in studentList
    {
      if (student.province == "BC"){
        studentList.removeValue(forKey:"key")
      }
    }
    for (_, student) in studentList
    {
      noBC.append(student.firstName + " " + student.lastName)
    }
    return noBC
  }
  // create a method "getAllStudentsFromABandBC"
  func getAllStudentsFromABandBC () -> [String] {
    var studentsABBC = [String] ()
    for (_, student) in studentList
    {
      if ((student.province) == "AB" || (student.province) == "BC"){
        let fullname = student.firstName + " " + student.lastName
        studentsABBC.append(fullname)
      }
    }
    return studentsABBC
  }
  
  // HELPERS TO FORMAT WIDTH of String and Int
  private func formatInt(int:Int, width:Int) -> String {
    let formattingString = "%" + String(width) + "d"
    return String(format: formattingString,int)
  }
  
  private func formatStringWidth(string:String, width:Int) -> String {
    let formattingString = "%" + String(width) + "s"
    let formattedString = String(format: formattingString,
                OpaquePointer(string.cString(using: String.Encoding.utf8)!))
    return formattedString
  }
  
}







