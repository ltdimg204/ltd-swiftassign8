//
//  Generator.swift
//  fn-assign8
//
//  Created by Frank  Niscak on 2016-11-10.
//  Copyright © 2016 NIC. All rights reserved.
//

import Foundation
import Fakery

public class Generator
{
 
  
  public static let professions: [String] =
    ["FullTime Student", "PartTime Student",
    "Instructor", "PartTime Instructor", "Administrator", "Janitor",
    "International Student", "Librarian", "Office Worker"];
  
  var faker: Faker
  
  init () {
   faker = Faker(locale: "en-CA")
  
  }
  
  public func getFirstName() -> String
  {     
    return faker.name.firstName()
  }
  
  public func getLastName() -> String
  {
    return faker.name.lastName()
  }
  
  public func getBirthYear() -> String
  {
    let birthYear = String(faker.number.randomInt(min:1900, max:2005))
    return birthYear
  }
  
  public func getStreet() -> String
  {
    return faker.address.streetName()
  }
  
  public func getStreetNumber() -> String
  {
    let streetNumb = String(faker.number.randomInt(min:1, max:2000))
    return streetNumb
  }
  
  public func getCity() -> String
  {
   return faker.address.city()
  }
  
  public func getProvince() -> String
  {
    return faker.address.stateAbbreviation()
  }
  
  public func getProfession() -> String
  {
    let count:Int = Generator.professions.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return Generator.professions[rindex]
  }
  
  
  public  func md5(string:String) -> String!
  {
    let str = string.cString(using: String.Encoding.utf8)
    let strLen = CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8))
    let digestLen = Int(CC_MD5_DIGEST_LENGTH)
    let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
    
    CC_MD5(str!, strLen, result)
    
    let hash = NSMutableString()
    for i in 0..<digestLen
    {
      hash.appendFormat("%02x", result[i])
    }
    
    result.deallocate(capacity: digestLen)
    return String(format: hash as String)
  }
  
  public func generatePersonRecord() -> String
  {
    let firstName:String = self.getFirstName()
    let lastName:String = self.getLastName()
    let birthYear:String = self.getBirthYear()
    let street:String = self.getStreet()
    let streetNumber:String = self.getStreetNumber()
    let city:String = self.getCity()
    let province:String = self.getProvince()
    
    // create a combined string to calculate MD5
    var totalString:String = ""
    totalString += firstName
    totalString += lastName
    totalString += birthYear
    totalString += street
    totalString += streetNumber
    totalString += city
    totalString += province
    
    let numberOfGrades = Int(arc4random_uniform(15))
    var strGrades:String = ""
    for _ in 0...numberOfGrades
    {
      let grade = String(arc4random_uniform(50) + 50)
      strGrades = strGrades +  "|\(grade)"
      
    }
    
    let md5String:String = self.md5(string: firstName + lastName + birthYear)
    
    let strRecord:String = String(format:"%@|%@|%@|%@|%@|%@|%@|%@%@", md5String,
                                  firstName, lastName, birthYear, street,
                                  streetNumber, city, province, strGrades)
    
    return strRecord;
  }
  
  public func generateMultiplePersonRecords(howMany:Int) -> [String]
  {
    var array: [String] = []
    
    for _ in 0 ..< howMany
    {
      array.append(self.generatePersonRecord())
    }
    return array;
  }
  
  
}
