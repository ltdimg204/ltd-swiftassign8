//
//  ViewController.swift
//  fn-assign8
//
//  Created by Frank  Niscak on 2016-11-10.
//  Copyright © 2016 NIC. All rights reserved.
//

import UIKit



class ViewController: UIViewController,  UIPickerViewDelegate,
UIPickerViewDataSource, UITableViewDelegate , UITableViewDataSource {

  @IBOutlet weak var actionPicker: UIPickerView!
  var actions:[String] = []
  var dispatchTable:[String: () ->[String]] = [:]
  
  @IBOutlet weak var myTableView: UITableView!
  var tableViewData:[String] = []
  
  // create a model object from the ApplicationModel class
  let model = ApplicationModel()
  
  //var textLines: [String] = [ "one", "two", "three"]
  // rename textLines into tableViewData
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    // PickerView - Connect delegate and data source:
    actionPicker.delegate = self
    actionPicker.dataSource = self
    // Initialize the dispatch table
    actions = ["Raw Data", "getSortedArrayOfAllCities",
    "getSortedArrayOfAllProvinces",
    "getAllStudentsFromComox","getStudentsSortedByLastName",
    "getStudentsSortedByAverageGrade","get3BestStudents",
    "get3WeakestStudents",
    "getStudentsSortedByAge",
    "getStudentsFromBC",
    "getAllStudentsFromABandBC",
    "removeAllStudentsFromBC",
    "getCitiesSortedByNumberOfStudents"]
    dispatchTable["Raw Data"] = model.generateNewData
    dispatchTable["getSortedArrayOfAllCities"] =
      model.getSortedArrayOfAllCities
    dispatchTable["getSortedArrayOfAllProvinces"] =
      model.getSortedArrayOfAllProvinces
    dispatchTable["getAllStudentsFromComox"] =
      model.getAllStudentsFromComox
    dispatchTable["getStudentsSortedByLastName"] =
      model.getStudentsSortedByLastName
    dispatchTable["getStudentsSortedByAverageGrade"] =
      model.getStudentsSortedByAverageGrade
    dispatchTable["get3BestStudents"] =
      model.get3BestStudents
    dispatchTable["get3WeakestStudents"] =
      model.get3WeakestStudents
    dispatchTable["getStudentsSortedByAge"] =
      model.getStudentsSortedByAge
    dispatchTable["getStudentsFromBC"] =
      model.getAllStudentsFromBC
    dispatchTable["getAllStudentsFromABandBC"] =
      model.getAllStudentsFromABandBC
    dispatchTable["removeAllStudentsFromBC"]=model.removeAllStudentsFromBC
    dispatchTable["getCitiesSortedByNumberOfStudents"] =
      model.getCitiesSortedByNumberOfStudents
    
    
    
    // TableView - Connect delegate and data source:
    myTableView.delegate = self
    myTableView.dataSource = self
    
    tableViewData = [ "one", "two", "three", "four", "five", "six",
                      "seven", "eight", "nine", "ten"]
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  /////////   PICKERVIEW SECTION ///////////
  // PickerView - The number of columns
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  // PickerView - The number of rows of data
  func pickerView(_ pickerView: UIPickerView,
                  numberOfRowsInComponent component: Int) -> Int {
    return actions.count
  }
  
  // PickerView -  data to return for the row and component 
  //(column) that's being passed in
  func pickerView(_ pickerView: UIPickerView,
      titleForRow row: Int, forComponent component: Int) -> String? {
    return actions[row]
  }
  
  // PickerView - react to clicked item of PickerView
  func pickerView(_ pickerView: UIPickerView,
      didSelectRow row: Int, inComponent component: Int) {
    let selectedText:String = actions[row]
    print(selectedText)
  }

  
  /////////   TABLEVIEW SECTION ///////////
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  // TableView - The number of rows of data
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    return tableViewData.count
  }
  
  // TableView - provide data for a given row (indexPath)
  func tableView(_ tableView: UITableView,
      cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier",
                                             for: indexPath)
    
    // Fetch Column and Row
    //let row = tableViewData[indexPath.row]
    let row = indexPath.row
    
    cell.textLabel?.font = UIFont (name: "Menlo-Regular", size: 11)
    
    // Configure Cell = Store Formatted Text into the cell
    cell.textLabel?.text = tableViewData[row]
    return cell
  }
  
  @IBAction func clickedExecute(_ sender: Any) {
    print(actions[actionPicker.selectedRow(inComponent: 0)])
    let index = actionPicker.selectedRow(inComponent: 0)
    let action = actions[index]
    
    if let executeAction = dispatchTable[action] {
      tableViewData = executeAction()
    } else {
      print("This action does not exist")
    }
    
    // display new data in the table view
    self.myTableView.reloadData()
  }

}

