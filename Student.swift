//
//  Student.swift
//  fn-assign8
//
//  Created by Frank  Niscak on 2016-11-10.
//  Copyright © 2016 NIC. All rights reserved.
//

import Foundation

class Student
{
  private var _md5: String = ""
  private var _firstName: String = ""
  private var _lastName: String = ""
  private var _birthYear: Int = 0
  private var _street :String = ""
  private var _streetNumber: Int = 0
  private var _city: String = ""
  private var _province: String = ""
  private var _grades: [Int] = []
  
  // create a willSet and didSet property Observers for stored property age

  var age: Int = 0 {
    
    
    willSet(newValue){
      print("Old value is \(age), new value is \(newValue)")
    }
    
    didSet {
      print("Old value is \(oldValue), new value is \(age)")
    }
    
  }     
  // failable designated initializer
  // create a failable designated initializer that initializes all
  // properties of this class (be sure that meaningful values are used
  // to initialize all properties)
  
  init?(md5:String, firstName:String, lastName:String, birthYear:Int,
        street:String, streetNumber:Int, city:String, province:String,
        grades:[Int])
  {
    if firstName.isEmpty || lastName.isEmpty || birthYear < 1900 ||
      street.isEmpty || streetNumber < 0 || city.isEmpty ||
      province.isEmpty || md5.isEmpty
    {
      return nil
    }
    self._firstName = firstName
    self._lastName = lastName
    self._birthYear = birthYear
    self._street = street
    self._streetNumber = streetNumber
    self._city = city
    self._province = province
    self._md5 = md5
    
    if grades.isEmpty
    {
      self._grades = []
    }
    else
    {
      self._grades = grades
    }
  }
  
  // computed properties
  var md5:String {
    return self._md5
  }
  
  var firstName:String {
    return self._firstName
  }
  
  var lastName:String {
    return self._lastName
  }
  
  var birthYear:Int {
    return self._birthYear
  }
  
  var street:String {
    return self._street
  }
  
  var streetNumber:Int {
    return self._streetNumber
  }
  
  var city:String {
    return self._city
  }
  
  var province:String {
    return self._province
  }
  
  var grades:[Int] {
    return self._grades
  }
  
}
